def calc_film_price(price, discount):
    """
    Функция расчёта цены с учётом скидки.

    Параметры:
    price (float): Цена;
    discount (int): Скидка.

    Возвращает:
    str: в случае ошибки;
    int (total_price): с результатом работы функции.

    Примеры:
    >>> calc_film_price(1000, 10)
    900
    >>> calc_film_price(123123.0, 30)
    86186
    """

    price, discount = float(price), int(discount)

    if price <= 0:
        return "Цена меньше или равна нулю"
    elif discount > 100:
        return "Скидка больше 100%"
    elif discount <= 0:
        "Скидка меньше или равна нулю"

    if discount >= 50:
        discount = 50

    total_price = round(price - (price * discount / 100))

    return int(total_price)


print(calc_film_price(input(), input()))
